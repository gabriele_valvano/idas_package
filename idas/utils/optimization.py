import tensorflow as tf


def train_op_wrapper(loss_function, optimizer=None, clip_grads=False, clip_value=5.0, var_list=None):
    """ Wrapper to train operation. If no optimizer is provided, it defaults to Adam. It allows for gradient
    clipping and definition of variables to optimize.
    ----
    Example:
        lr = 1e-4
        adam_opt = tf.train.AdamOptimizer(lr)
        vars = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, "MyModel")
        train_op = train_op_wrapper(loss_function, adam_opt, clip=True, clip_value=0.5, var_list=sup_vars)
    """

    if optimizer is None:
        optimizer = tf.train.AdamOptimizer()

    # define update_ops to update batch normalization population statistics
    update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)

    with tf.control_dependencies(update_ops):
        # gradient clipping for stability:
        gradients, variables = zip(*optimizer.compute_gradients(loss_function, var_list))
        if clip_grads:
            gradients, _ = tf.clip_by_global_norm(gradients, clip_value)
        # train op:
        train_op = optimizer.apply_gradients(zip(gradients, variables), global_step=self.g_train_step)

    return train_op
